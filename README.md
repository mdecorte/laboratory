# Laboratory Projects
Personal play yard for web

## Festival Timetable 

### What is it:
Create festival timetable from JSON data and be able to create (and export?) a personalized Timetable.

### Techniques:
- Webpack
- css next
- service worker (offline)
- React?

### Work it:
Install dependencies:  
`$ yarn`  

Run server:  
`$ yarn run dev`
  
Navigate to:  
`$ http://localhost:8080/webpack-dev-server/index.html`

### TODO:

- [x] Timetable
- [x] Program overview from JSON
- [x] Get data from BKS
- [x] getArtist() function for podium columns OR map and enrich stage objects with acts
- [ ] Use CSSNext at potential (variables in DOM)
- [ ] Add/link images to data
- [ ] Think of something else but top-ofset for placing tt-artist-block
- [ ] Make items selectable (more info + add to favourites)
- [ ] Use SW for offline functionality
- [ ] Drag & drop functionality?

