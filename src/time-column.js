import { createTime } from './utils';

export default function(startTime, endTime) {
    const div = document.createElement('div');
    div.classList.add('time-column');

    const quarters = ((endTime - startTime) * 0.04);

    let q = startTime;
    let hour = 0;
    let i = 0;
    for (i, quarters; i < quarters; i++) {

        const bla =  createTime(q).toString().split('');
        bla.splice(2, 0, ':');
        const time = bla.join('');

        div.innerHTML += `<div class="quarter">${time}</div>`;
        q += 15;
        hour += 15;
        if (hour === 60) {
            q += 40;
            hour = 0;
        }
        if (q === 2400) {
            q = 0;
        }
    }
    return div;
}
