function mathify(startTime, endTime, stageStartTime) {

    let start = Math.round(startTime);
    let end = Math.round(endTime);

    if (stageStartTime) {
        if (start < stageStartTime) {
            start += 2400;
            end += 2400;
        }
    }

    if (end < start || end === 0) {
        end += 2400;
    }

    if (stageStartTime) {
        start -= stageStartTime;
        end -= stageStartTime;
    }

    return {start, end};
}

function createTime(numb) {

    const length = numb.toString().length;

    if (length === 1) {
        return `000${numb}`;
    } else if (length === 2) {
        return `00${numb}`;
    } else if (length === 3) {
        return `0${numb}`;
    } else if (length === 4) {
        return numb;
    }

}

export {mathify, createTime}
