import { mathify, createTime } from './utils';
import artists from './artists.json';

export default function(artist, eventStart, eventEnd, areaStartTime) {



    const div = document.createElement('div');
    div.classList.add('artist-block');
    artists.forEach(artistData => {
        if (artistData.name == artist) {
            div.classList.add('has-image');
            div.classList.add('img-active');
            div.addEventListener('click', e => {
                div.style.backgroundImage = `url(${artistData.images[0]})`;
                div.classList.toggle('img-active');
            })
        }
    });
    const {start, end} = mathify(eventStart, eventEnd, areaStartTime);
    const bla =  createTime(eventStart).toString().split('');
    bla.splice(2, 0, ':');
    const time = bla.join('');
    const bloe =  createTime(eventEnd).toString().split('');
    bloe.splice(2, 0, ':');
    const time2 = bloe.join('');
    let elHeight = end - start;
    if (elHeight > 60 && elHeight <= 120) {
        elHeight -= 40;
    }
    if (elHeight > 120 && elHeight <= 180) {
        elHeight -= 80;
    }
    if (elHeight > 180 && elHeight <= 240) {
        elHeight -= 120;
    }
    const realHeight = createTime(start).toString().split('');
    const minutes = Math.floor((Number(realHeight[0] + realHeight[1]) * 60 + Number(realHeight[2] + realHeight[3])));
    const color = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
    div.style = `margin-top:${ minutes * 2 }px;height:${(elHeight * 2)}px;background-color:${color};`;

    div.innerHTML = `${artist} (${time})`;

    return div;

}
