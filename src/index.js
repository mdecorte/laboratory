import './style.pcss';
import json from './days.json';
import artistBlock from './tt-artist-block';
import timeColumn from './time-column';
import { mathify } from './utils';

// TODO: create time column (start-time, end-time)

function TimeTableElement() {

    const timeTable = document.createElement('div');
    timeTable.classList.add('time-table');

    const [startTime, endTime] = [json['day1'].time.start, json['day1'].time.end];
    const {start, end} = mathify(startTime, endTime);

    json['day1'].areas.forEach(area => {
        const acts = getActs(area);
        const columnElement = createColumn(acts);
        timeTable.appendChild(columnElement);
    });

    function createColumn(acts) {
        const div = document.createElement('div');
        div.classList.add('column');
        acts.forEach(act => {
            div.appendChild(artistBlock(act.title, act.start, act.end, startTime))
        });
        return div;
    }

    function getActs(area) {
        return json['day1'].events.filter(e => e.area === area.ID);
    }

    timeTable.appendChild(timeColumn(start, end));

    return timeTable;
}

document.body.appendChild(TimeTableElement());
